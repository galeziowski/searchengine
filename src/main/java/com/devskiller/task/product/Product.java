package com.devskiller.task.product;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Product { // TODO make me immutable

    private final Long id;

    private final Map<String, String> stringProperties;

    private final Map<String, Integer> integerProperties;

    public Product(Long id, Map<String, String> stringProperties, Map<String, Integer> integerProperties) {
        this.id = id;
         
        HashMap<String,String> stringPropertiesDeepCopy = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : stringProperties.entrySet())
        {
        	stringPropertiesDeepCopy.put(entry.getKey(),
               entry.getValue());
        }
        this.stringProperties = stringPropertiesDeepCopy;
        HashMap<String,Integer> integerPropertiesDeepCopy = new HashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : integerProperties.entrySet())
        {
        	integerPropertiesDeepCopy.put(entry.getKey(),
               entry.getValue());
        }
        this.integerProperties = integerPropertiesDeepCopy;
    }

    public Optional<String> getStringProperty(String propertyName) {
        return Optional.ofNullable(stringProperties.get(propertyName));
    }

    public Optional<Integer> getIntegerProperty(String propertyName) {
        return Optional.ofNullable(integerProperties.get(propertyName));
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", stringProperties=" + stringProperties +
                ", integerProperties=" + integerProperties +
                '}';
    }
}
