package com.devskiller.task.product;

import java.util.List;

public class ProductSet {

    private final List<Product> products;

    public ProductSet(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }
}
