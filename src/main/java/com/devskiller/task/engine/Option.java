package com.devskiller.task.engine;

import com.devskiller.task.product.Product;

import java.util.List;
import java.util.function.Predicate;

public class Option implements Predicate<Product> { // FIXME

	private Long id;

	private List<Predicate<Product>> predicates;

	public Option(Long id, List<Predicate<Product>> predicates) {
		this.id = id;
		if (predicates.isEmpty())
			throw new IllegalArgumentException();
		this.predicates = predicates;
	}

	public Long getId() {
		return id;
	}

	@Override
	public boolean test(Product product) {
		return predicates.stream().allMatch(f -> f.test(product));

	}
}
