package com.devskiller.task.engine;

import com.devskiller.task.product.Product;
import com.devskiller.task.product.ProductSet;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SearchEngine {

    public List<Product> search(ProductSet productSet, List<Option> selectedOptions) {
        
    	List<Product> outputList = new ArrayList<Product>();
    	List<Product> products = productSet.getProducts();

    	for (Product product : products) {
    		if (selectedOptions.stream().allMatch(f -> f.test(product))) {
    			outputList.add(product);
    		};   		
    	}
    	
        return outputList;
    }
}
