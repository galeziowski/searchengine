package com.devskiller.task.predicate;

import com.devskiller.task.product.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CompoundPredicate implements Predicate<Product> {
    private Relation relation;
    private List<Predicate<Product>> predicates;

    public CompoundPredicate(Relation relation, List<Predicate<Product>> predicates) {
        this.relation = relation;
        if (predicates.isEmpty() || predicates.size()==1)
			throw new IllegalArgumentException();
        this.predicates = new ArrayList<>(predicates);
    }

    @Override
    public boolean test(Product product) {
    	switch(relation) {
    	case OR : return predicates.stream().anyMatch((f -> f.test(product)));
    	case AND: return predicates.stream().allMatch(f -> f.test(product));
    	default:
            throw new UnsupportedOperationException("Unknown relation: " + relation.name());
    	}
    }

    public enum Relation {
        AND, OR
    }
}