package com.devskiller.task.predicate;

import com.devskiller.task.product.Product;

import java.util.Optional;
import java.util.function.Predicate;

public class IntegerPredicate implements Predicate<Product> {

    private String propertyName;

    private IntegerOperator operator;

    private int referenceValue;

    public IntegerPredicate(String propertyName, IntegerOperator operator, int referenceValue) {
        this.propertyName = propertyName;
        this.operator = operator;
        this.referenceValue = referenceValue;
    }

    @Override
    public boolean test(Product product) {
        Optional<Integer> property = product.getIntegerProperty(propertyName);

        if (property.isEmpty()) return false;
        
        int value = property.get();

        switch (operator) {
            case LESS_THAN:
                return value < referenceValue;
            case LESS_OR_EQUAL_THAN:
                return value <= referenceValue;
            case EQUAL:
                return value == referenceValue;
            case NOT_EQUAL:
                return value != referenceValue;
            case GREATER_THAN:
                return value > referenceValue;
            default:
                throw new UnsupportedOperationException("Unknown operator: " + operator.name());
        }
    }

    public enum IntegerOperator {
        LESS_THAN,
        LESS_OR_EQUAL_THAN,
        EQUAL,
        NOT_EQUAL,
        GREATER_THAN,
        GREATER_OR_EQUAL_THAN
    }
}
