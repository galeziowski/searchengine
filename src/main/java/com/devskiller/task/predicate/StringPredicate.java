package com.devskiller.task.predicate;

import com.devskiller.task.product.Product;

import java.util.Optional;
import java.util.function.Predicate;

public class StringPredicate implements Predicate<Product> {

    private String propertyName;

    private StringOperator operator;

    private String referenceValue;

    public StringPredicate(String propertyName, StringOperator operator, String referenceValue) {
        this.propertyName = propertyName;
        this.operator = operator;
        this.referenceValue = referenceValue;
    }

    @Override
    public boolean test(Product product) {
    	Optional<String> property = product.getStringProperty(propertyName);
    	
    	if (property.isEmpty()) return false;
    	String  value = property.get();
    	
    	switch (operator) {
        case EQUAL: return value.equals(referenceValue);
        case NOT_EQUAL : return !value.equals(referenceValue);
        default:
            throw new UnsupportedOperationException("Unknown operator: " + operator.name());
    	}
    }

    public enum StringOperator {
        EQUAL,
        NOT_EQUAL,
    }
}
