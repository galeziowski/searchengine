package com.devskiller.task.product;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class ProductTest {

    @Test
    public void shouldBeImmutable() {
        // given
        HashMap<String, Integer> integerMap = new HashMap<>();
        HashMap<String, String> stringMap = new HashMap<>();

        integerMap.put("x", 10);
        stringMap.put("y", "value");

        Product product = new Product(1L, stringMap, integerMap);

        // when
        integerMap.clear();
        stringMap.put("y", "other");

        // then
        assertEquals(10, product.getIntegerProperty("x").get().intValue());
        assertEquals("value", product.getStringProperty("y").get());
    }
}