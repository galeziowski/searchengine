package com.devskiller.task.engine;

import com.devskiller.task.product.Product;
import com.devskiller.task.product.ProductSet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchEngineTest {

    private SearchEngine engine = new SearchEngine();
    private ProductSet productSet;
    private List<Option> options;

    @Before
    public void setUp() {
        productSet = createDummyProductSet();
        options = createExampleOptions();
    }

    @Test
    public void shouldReturnAllProductsWhenNoOptionsSelected() {
        // when
        List<Product> matchingProducts = engine.search(productSet, new ArrayList<>());

        // then
        assertEquals(productSet.getProducts().size(), matchingProducts.size());
    }

    @Test
    public void shouldReturnProperProductsForConjunctionOfSelectedOptions() {
        // when
        List<Product> matchingProducts = engine.search(productSet, selectOptions(0, 2));

        // then
        assertTrue(matchingProducts.contains(productSet.getProducts().get(1)));
        assertTrue(matchingProducts.contains(productSet.getProducts().get(2)));
        assertEquals(2, matchingProducts.size());
    }

    private List<Option> createExampleOptions() {
        List<Option> options = new ArrayList<>();
        options.add(createOptionMock(100L, selectProducts(0, 1, 2)));
        options.add(createOptionMock(101L, selectProducts(4)));
        options.add(createOptionMock(102L, selectProducts(1, 2, 3)));
        return options;
    }

    private List<Option> selectOptions(int... indexes) {
        return stream(indexes).mapToObj(i -> options.get(i)).collect(toList());
    }

    private List<Product> selectProducts(int... indexes) {
        List<Product> products = productSet.getProducts();
        return stream(indexes).mapToObj(products::get).collect(toList());
    }


    private ProductSet createDummyProductSet() {
        List<Product> products = new ArrayList<>();
        products.add(new Product(10L, new HashMap<>(), new HashMap<>()));
        products.add(new Product(11L, new HashMap<>(), new HashMap<>()));
        products.add(new Product(12L, new HashMap<>(), new HashMap<>()));
        products.add(new Product(13L, new HashMap<>(), new HashMap<>()));
        products.add(new Product(14L, new HashMap<>(), new HashMap<>()));
        return new ProductSet(products);
    }

    private Option createOptionMock(Long optionId, List<Product> matchingProducts) {
        Option option = mock(Option.class);
        when(option.getId()).thenReturn(optionId);
        when(option.test(any(Product.class))).thenAnswer((Answer<Boolean>) invocationOnMock -> {
            Product product = invocationOnMock.getArgumentAt(0, Product.class);
            return matchingProducts.contains(product);
        });
        return option;
    }
}