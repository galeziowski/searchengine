package com.devskiller.task.engine;

import com.devskiller.task.product.Product;
import org.junit.Test;

import java.util.ArrayList;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class OptionTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionIfEmptyPredicatesAdded() {
        // when
        new Option(1L, new ArrayList<>());
    }

    @Test
    public void shouldNotAcceptProductWithNotMatchingPredicate() {
        // given
        Option option = new Option(1L, asList(product -> false));
        Product product = mock(Product.class);

        // when
        boolean result = option.test(product);

        // then
        assertFalse(result);
    }

    @Test
    public void shouldAcceptProductWithMatchingPredicates() {
        // given
        Option option = new Option(1L, asList(product -> true, product -> true));
        Product product = mock(Product.class);

        // when
        boolean result = option.test(product);

        // then
        assertTrue(result);
    }
}