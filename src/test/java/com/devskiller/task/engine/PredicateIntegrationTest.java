package com.devskiller.task.engine;

import com.devskiller.task.predicate.CompoundPredicate;
import com.devskiller.task.predicate.IntegerPredicate;
import com.devskiller.task.predicate.IntegerPredicate.IntegerOperator;
import com.devskiller.task.predicate.StringPredicate;
import com.devskiller.task.predicate.StringPredicate.StringOperator;
import com.devskiller.task.product.Product;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.devskiller.task.predicate.CompoundPredicate.Relation.AND;
import static com.devskiller.task.predicate.CompoundPredicate.Relation.OR;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PredicateIntegrationTest {

    @Test
    public void shouldTestProductsWithSimpleIntegerPredicate() {
        // given
        IntegerPredicate integerPredicate = getIntegerPredicate(IntegerOperator.GREATER_THAN, 5);

        Product product1 = getFirstProduct();
        Product product2 = getSecondProduct();

        // then
        assertFalse(integerPredicate.test(product1));
        assertTrue(integerPredicate.test(product2));
    }

    @Test
    public void shouldTestProductsWithSimpleStringPredicate() {
        // given
        StringPredicate stringPredicate = getStringPredicate(StringOperator.EQUAL, "some");

        Product product1 = getFirstProduct();
        Product product2 = getSecondProduct();

        // then
        assertFalse(stringPredicate.test(product1));
        assertTrue(stringPredicate.test(product2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForCompoundPredicateWithNoPredicates() {
        // when
        new CompoundPredicate(AND, emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForCompoundPredicateWithOnePredicate() {
        // when
        new CompoundPredicate(AND, asList(getStringPredicate(StringOperator.EQUAL, "some")));
    }

    @Test
    public void shouldTestProductsWithCompoundPredicate() {
        // given
        IntegerPredicate integerPredicate = getIntegerPredicate(IntegerOperator.GREATER_THAN, 5);
        StringPredicate stringPredicate = getStringPredicate(StringOperator.EQUAL, "some");

        CompoundPredicate compoundPredicateWithAlternative = new CompoundPredicate(OR, asList(integerPredicate, stringPredicate));

        Product product1 = getFirstProduct();
        Product product2 = getSecondProduct();
        Product product3 = getThirdProduct();

        // then
        assertTrue(compoundPredicateWithAlternative.test(product3));
        assertTrue(compoundPredicateWithAlternative.test(product2));
        assertFalse(compoundPredicateWithAlternative.test(product1));
    }

    @Test
    public void shouldReturnFalseForStringPredicateWhichIsMissingInProduct() {
        // given
        StringPredicate stringPredicate = new StringPredicate("unknown", StringOperator.EQUAL, "some");

        Product product = getFirstProduct();

        // then
        assertFalse(stringPredicate.test(product));
    }

    private IntegerPredicate getIntegerPredicate(IntegerOperator operator, int referenceValue) {
        return new IntegerPredicate("abc", operator, referenceValue);
    }

    private StringPredicate getStringPredicate(StringOperator operator, String referenceValue) {
        return new StringPredicate("def", operator, referenceValue);
    }

    private Product getFirstProduct() {
        Map<String, Integer> integerProperties = new HashMap<>();
        Map<String, String> stringProperties = new HashMap<>();
        integerProperties.put("abc", 2);
        stringProperties.put("def", "other");
        return new Product(10L, stringProperties, integerProperties);
    }

    private Product getSecondProduct() {
        Map<String, Integer> integerProperties = new HashMap<>();
        Map<String, String> stringProperties = new HashMap<>();
        integerProperties.put("abc", 6);
        stringProperties.put("def", "some");
        return new Product(11L, stringProperties, integerProperties);
    }

    private Product getThirdProduct() {
        Map<String, Integer> integerProperties = new HashMap<>();
        Map<String, String> stringProperties = new HashMap<>();
        integerProperties.put("abc", 5);
        stringProperties.put("def", "some");
        return new Product(12L, stringProperties, integerProperties);
    }
}